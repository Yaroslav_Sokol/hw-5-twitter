const request = async (method, url, body = null) => {
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const response = await axios({
            url: url,
            headers: headers,
            method: method,
            data: body,
        });

        if (method !== 'DELETE') {
            return response.data;
        } else {
            return response;
        }
        
    } catch (error) {
        console.error(error);
        throw error;
    }
};

export default request;