import request from "../api/request.js";
import deletePost from "../function/postsDelete.js";

class Card {
    constructor(urlUser, urlPost) {
        this.urlUser = urlUser;
        this.urlPost = urlPost;
    }

    async listCard() {
        try {
            const users = await request('GET', this.urlUser);
            const posts = await request('GET', this.urlPost);
            const postContainer = document.querySelector('.post-container');

            const contentCards = posts.map(
                ({ id, userId, title, body }) =>
                `<div class = "card__post-${id}">
                    <div class = "card">
                        <div class = "card__header">
                            <div class = "card__header_title">
                                <img class = "card__logo" src = "../img/user.png" alt = "User">
                                <h1>${users.find(user => user.id === userId).name}</h1>
                                <h3>${users.find(user => user.id === userId).email}</h3>
                            </div>
                            <button class = "card__post_btn" data-id = ${id}>Delete</button>
                        </div>
                        <h2>${title}</h2>
                        <p>${body}</p>
                    </div>
                    <hr>
                </div>`
            );

            postContainer.innerHTML = contentCards.join('');


            const deleteButtons = document.querySelectorAll('.card__post_btn');
            deleteButtons.forEach(button => {
                button.addEventListener('click', async () => {
                    const postId = button.getAttribute('data-id');
                    await deletePost(postId, this.urlPost);
                });
            });
        } catch (error) {
            console.error(error);
        }
    }
}

export default Card;