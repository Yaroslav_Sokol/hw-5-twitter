import request from "../api/request.js";

const deletePost = async (postId, urlPost) => {
    try {
        const response = await request('DELETE', `${urlPost}/${postId}`);

        if (response && response.status === 200) {
            const removePost = document.querySelector(`.card__post-${postId}`);
            if (removePost) {
                removePost.remove();
            }
        } else {
            console.error('Error');
        }
    } catch (error) {
        console.error(error);
    }
};

export default deletePost;
